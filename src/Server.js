const { Server, Origins } = require('boardgame.io/server');
const { EntanglementChess } = require('./Game');

const server = Server({
  games: [EntanglementChess],
  origins: [
    '*',
    'https://entanglement-chess.netlify.app',
    Origins.LOCALHOST
  ],
});

server.run(process.env.PORT || 8000);