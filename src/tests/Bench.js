import { Client } from 'boardgame.io/client';
import { EntanglementChess } from '../Game';

const bench = (moveList) => {
  const start = Date.now();
  let moveCount = 0;
  const getInfo = (client) => {
    let state = client.getState();
    if(state !== null) {
      moveCount += state.G.moves.length;
    }
  }
  const client = Client({ game: EntanglementChess, debug: false });
  getInfo(client);
  for(const move of moveList) {
    client.moves.movePiece(move);
    getInfo(client);
  }
  console.log(`Available Moves Generated: ${moveCount}`);
  console.log(`Benchmark took: ${(Date.now() - start)/1000} seconds`);
}

bench(["a2a4",
"a7a5",
"b2b4",
"b7b5",
"c2c4",
"c7c5",
"d2d4",
"d7d5",
"e2e4",
"e7e5",
"f2f4",
"f7f5",
"g2g4",
"g7g5",
"h2h4",
"h7h5",
"h1f3",
"h8f6",
"g1h1",
"f6h6",
"f3h3",
"g8h8",
"f1g1",
"f8g8",
"g1g3",
"g8g6",
"h1f1",
"h8f8",
"e1c3",
"e8f6",
"d1e3",
"g5h4",
"g4g5",
"h4g3",
"h3g3",
"e5f4",
"g5h6",
"f4g3",
"a1b3",
"c8e6",
"e3f5",
"b8e5",
"f5g3",
"g6h6",
"g3h5",
"a5b4",
"a4b5",
"c5d4",
"c3b4",
"d5c4",
"b3d4",
"f6h5",
"d4f3",
"e6d5",
"f3e5",
"h5f6",
"e4d5",
"f6d5",
"e5c4",
"d5b4",
"f1f6",
"b4d5",
"b5b6",
"d5b6",
"f6h6",
"f8f6",
"h6f6",
"b6d5",
"c4e3",
"d5f6",
"c1d2",
"f6h5",
"b1a2",
"h5g7",
"d2c3",
"g7h5",
"a2b3"]);
