import { Client } from 'boardgame.io/client';
import { EntanglementChess } from '../Game';

const { Chess } = require('chess.js');
const deepcopy = require('deepcopy');

const reducePPs = (PPs) => {
  let randomPP = PPs[Math.floor(Math.random() * PPs.length)];
  if(Array.isArray(randomPP)) {
    return randomPP[Math.floor(Math.random() * randomPP.length)]
  }
  return randomPP;
}
const reduceG = (G) => {
  for(let r = 0;r < 8;r++) {
    for(let f = 0;f < 8;f++) {
      if(G.board[r][f] !== null) {
        G.board[r][f] = reducePPs(G.board[r][f]);
      }
    }
  }
  for(let i = 0;i < G.captured.length;i++) {
    G.captured[i] = reducePPs(G.captured[i]);
  }
}

const revertG = (G) => {
  for(let i = G.moveHistory.length - 1;i >= 0;i--) {
    const currMove = G.moveHistory[i];
    let destR = Number(currMove.substr(3,1)) - 1;
    let destF = currMove.charCodeAt(2) - 97;
    let srcR = Number(currMove.substr(1,1)) - 1;
    let srcF = currMove.charCodeAt(0) - 97;
    G.board[srcR][srcF] = '' + G.board[destR][destF];
    if(G.moveHistoryWasCapture[i]) {
      G.board[destR][destF] = G.captured.pop();
    }
    else {
      G.board[destR][destF] = null;
    }
  }
}

const mapGChess = (G) => {
  const chess = new Chess();
  chess.clear();
  for(let r = 0;r < 8;r++) {
    for(let f = 0;f < 8;f++) {
      if(G.board[r][f] !== null) {
        chess.put({
          type: G.board[r][f].toLowerCase(),
          color: G.board[r][f] === G.board[r][f].toLowerCase() ? 'w' : 'b'
        }, String.fromCharCode(97 + f) + (r + 1));
      }
    }
  }
  return chess;
}

const validateGame = (chess, G) => {
  for(let move of G.moveHistory) {
    try {
      chess.move({ from: move.substr(0,2), to: move.substr(2,2) });
    }
    catch(err) {
      console.error(err);
      return false;
    }
  }
  return true;
}

while(true) {
  console.info('Starting new Entanglement Chess game');
  let gameDone = false;
  let movesPlayed = 0;
  const client = Client({ game: EntanglementChess, debug: false });
  let state = client.getState();
  while(!gameDone) {
    state = client.getState();
    while(state === null) {
      state = client.getState();
    }
    let validMoves = state.G.validMoves;
    if(validMoves.length > 0) {
      let randomValidMove = validMoves[Math.floor(Math.random() * validMoves.length)];
      console.info(`Played move: ${randomValidMove.id}`)
      client.moves.movePiece(randomValidMove.id);
    }
    else {
      gameDone = true;
    }
    movesPlayed++;
    if(movesPlayed > 75) {
      gameDone = true;
    }
  }
  console.info('Ending game, mapping to random end position');
  let gameValid = false;
  let validationCount = 0;
  while(!gameValid) {
    let newG = deepcopy(state.G);
    reduceG(newG);
    revertG(newG);
    const chess = mapGChess(newG);
    console.info(chess.ascii());
    let validGame = validateGame(chess, newG);
    if(validGame) {
      console.info('Game validated');
      console.info(chess.ascii());
      gameValid = true;
    }
    else {
      console.info('Position invalid! Retrying with new random end position');
    }
    validationCount++;
    if(validationCount > 20) {
      console.info('Quitting validation attempt');
      gameValid = true;
    }
  }
}