import * as PIXI from 'pixi.js';
import { createNanoEvents } from 'nanoevents';
import Piece from '../../renderer/Piece';

const emitter = createNanoEvents();

const displayElement = document.getElementById('display');
const displayApp = new PIXI.Application({ width: 512, height: 512, backgroundAlpha: 0 });
displayElement.appendChild(displayApp.view);
let piece = new Piece(emitter, 0, 0, displayApp.stage, '', ['P','N','B','R','Q','K']);
piece.root.width = 512;
piece.root.height = 512;
piece.root.position.x = -17;
piece.root.position.y = -17;