import * as PIXI from 'pixi.js';
import { createNanoEvents } from 'nanoevents';
import { Client } from 'boardgame.io/client';
import { SocketIO } from 'boardgame.io/multiplayer';
import { EntanglementChess } from './Game';
import Board from './renderer/Board';
const base64json = require('base64json');

class EntanglementChessClient {
  constructor(clientOptions, rootElement, extraElement) {
    this.emitter = createNanoEvents();
    this.client = Client(clientOptions);
    window.client = this.client;
    this.client.start();
    this.rootElement = rootElement;
    this.extraElement = extraElement;
    this.app = new PIXI.Application({ width: 1240, height: 820, backgroundColor: 0x2b443c });
    this.selectSrc = null;
    this.board = null;
    this.initialize();
    this.listen();
  }
  initialize() {
    this.rootElement.appendChild(this.app.view);
    this.extraElement.innerHTML = `
      <p id="winner"></p>
      <div style="display: flex; width: 820px">
        <p style="width: 50%; max-height: 400px; overflow: auto;" id="moveHistory"></p>
        <div style="width: 50%">
          <p style="width: 100%">Load from move history</p>
          <textarea style="width: 100%" rows="20" id="moveHistoryInput"></textarea>
          <button style="width: 100%" id="moveHistoryButton">Load</button>
        </div>
      </div>
    `;
    let state = this.client.getState();
    if(state !== null) {
      this.update(state.G, state.ctx, state);
    }
  }
  update(G, ctx, state) {
    if(this.board) {
      this.board.update(G, ctx);
    }
    else {
      this.board = new Board(this.emitter, this.app.stage, G, ctx);
    }
    //Display board history
    let moveHistoryDisplay = document.getElementById('moveHistory');
    moveHistoryDisplay.innerText = 'Move History:\n';
    for(const move of G.moveHistory) {
      moveHistoryDisplay.innerText += move + '\n';
    }
    //Display winner
    let winnerElement = document.getElementById('winner');
    let displayText = '';
    if(state.isConnected) {
      displayText = 'Connected to server, waiting for opponent...';
      if(!this.client.playerID) {
        displayText = 'Spectating game!';
      }
      else if(this.client.matchData) {
        let allConnected = true;
        let allJoined = true;
        for(const player of this.client.matchData) {
          if(!player.isConnected) {
            allConnected = false;
          }
          if(!player.name) {
            allJoined = false;
          }
        }
        if(allConnected) {
          displayText = 'Opponent connected!';
        }
        else if(allJoined) {
          displayText = 'Opponent disconnected!';
        }
      }
    }
    else if(this.client.matchID) {
      displayText = 'Disconnected from server!';
    }
    if(ctx.gameover) {
      if(ctx.gameover.draw) {
        winnerElement.innerText = 'Draw!';
      }
      else {
        winnerElement.innerText = (ctx.gameover.winner === '0' ? 'White' : 'Black') + ' Wins!';
      }
    }
    else if(G.inCheck) {
      winnerElement.innerText = 'Check!';
    }
    else {
      winnerElement.innerText = displayText;
    }
  }
  loadMoveHistory() {
    const moveHistoryInput = document.getElementById('moveHistoryInput');
    const moveHistory = moveHistoryInput.value.split('\n');
    this.client.reset();
    for(const move of moveHistory) {
      this.client.moves.movePiece(move);
    }
  }
  ppString(PPs) {
    let res = '';
    for(const PP of PPs) {
      if(Array.isArray(PP)) {
        res += `[${PP.join('')}]`;
      }
      else {
        res += PP;
      }
    }
    return res;
  }
  print() {
    let state = this.client.getState();
    if(state !== null) {
      console.log('Board:');
      console.log(''.padEnd(9*8+1, '-'));
      for(let r = 7;r >= 0;r--) {
        let line = '|';
        for(let f = 0;f < 8;f++) {
          if(state.G.board[r][f] !== null) {
            line += ` ${this.ppString(state.G.board[r][f]).padEnd(6, ' ')} |`;
          }
          else{
            line += ` ${''.padEnd(6, ' ')} |`;
          }
        }
        console.log(line);
        console.log(''.padEnd(9*8+1, '-'));
      }
      console.log(`Captured: ${state.G.captured.map(PPs => `${this.ppString(PPs)}`).join(' ')}`);
    }
  }
  listen() {
    this.client.subscribe(state => {
      if(state !== null) {
        this.update(state.G, state.ctx, state);
      }
    });

    this.emitter.on('moveSelect', (move) => {
      let state = this.client.getState();
      if(state !== null) {
        if(state.isActive) {
          this.client.moves.movePiece(move);
        }
        else {
          this.update(state.G, state.ctx, state);
        }
      }
    });

    this.emitter.on('updateRequest', () => {
      let state = this.client.getState();
      if(state !== null) {
        this.update(state.G, state.ctx, state);
      }
    });

    const moveHistoryButton = document.getElementById('moveHistoryButton');
    moveHistoryButton.onclick = this.loadMoveHistory.bind(this);
  }
}

let clientOptions = { game: EntanglementChess, debug: false };
const params = new URLSearchParams(window.location.search);
if(params.has('data')) {
  const decodedObject = base64json.parse(params.get('data'));
  Object.assign(clientOptions, decodedObject);
  clientOptions.multiplayer = SocketIO({ server: decodedObject.server });
  console.log(clientOptions)
}
const appElement = document.getElementById('app');
const extraElement = document.getElementById('extra');
const app = new EntanglementChessClient(clientOptions, appElement, extraElement);
window.app = app;