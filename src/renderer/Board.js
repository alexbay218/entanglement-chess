import * as PIXI from 'pixi.js';
import Piece from './Piece';
import Highlight from './Highlight';

export default class Board {
  constructor(emitter, parentContainer, G, ctx) {
    this.emitter = emitter;
    this.root = new PIXI.Container();
    this.root.width = 1240;
    this.root.height = 820;
    parentContainer.addChild(this.root);
    this.graphics = new PIXI.Graphics();
    this.root.addChild(this.graphics);
    this.boardPieces = [];
    this.capturedPieces = [];
    this.highlights = [];
    this.selectedPiece = null;
    this.listen();
    this.update(G, ctx);
  }
  update(G, ctx) {
    //Draw board
    let isWhite = ctx.currentPlayer === '0';
    this.graphics.beginFill(isWhite ? 0xffffff : 0x000000);
    this.graphics.drawRoundedRect(0,0,820,820,5);
    for(let r = 0;r < 8;r++) {
      for(let f = 0;f < 8;f++) {
        this.graphics.beginFill(f % 2 !== r % 2 ? 0xa59265 : 0xffffdd);
        this.graphics.drawRect((f * 100) + 10, (r * 100) + 10, 100, 100);
      }
    }

    //Display board pieces
    for(let r = 0;r < 8;r++) {
      for(let f = 0;f < 8;f++) {
        if(Array.isArray(this.boardPieces[r]) && this.boardPieces[r][f]) {
          this.boardPieces[r][f].update(G.board[r][f]);
        }
        else {
          if(!Array.isArray(this.boardPieces[r])) {
            this.boardPieces[r] = [];
          }
          let id = `${String.fromCharCode(97 + f)}${r + 1}`;
          this.boardPieces[r][f] = new Piece(this.emitter, f * 100 + 10, 700 - (r * 100) + 10, this.root, id, G.board[r][f]);
        }
      }
    }

    //Display captured pieces
    for(let i = 0;i < G.captured.length;i++) {
      if(this.capturedPieces[i]) {
        this.capturedPieces[i].update(G.captured[i]);
      }
      else {
        let x = (Math.floor(i / 8) * 100) + 830;
        let y = (i % 8 * 100) + 10;
        this.capturedPieces[i] = new Piece(this.emitter, x, y, this.root, null, G.captured[i]);
      }
    }
    //Clear non captured
    for(let i = 0;i < this.capturedPieces.length;i++) {
      if(i >= G.captured.length) {
        this.capturedPieces[i].update(null);
      }
    }


    //Display board highlights
    for(let r = 0;r < 8;r++) {
      for(let f = 0;f < 8;f++) {
        let highlightType = null;
        let id = `${String.fromCharCode(97 + f)}${r + 1}`;
        if(this.selectedPiece !== null) {
          if(this.selectedPiece === id) {
            highlightType = {
              clickable: false
            };
          }
          for(let move of G.moves) {
            if(move.id.substr(0,2) === this.selectedPiece) {
              if(move.id.substr(2,2) === id) {
                highlightType = {
                  clickable: true
                };
              }
            }
          }
        }
        if(Array.isArray(this.highlights[r]) && this.highlights[r][f]) {
          this.highlights[r][f].update(highlightType);
        }
        else {
          if(!Array.isArray(this.highlights[r])) {
            this.highlights[r] = [];
          }
          let id = `${String.fromCharCode(97 + f)}${r + 1}`;
          this.highlights[r][f] = new Highlight(this.emitter, f * 100 + 10, 700 - (r * 100) + 10, this.root, id, highlightType);
        }
      }
    }
  }
  listen() {
    this.emitter.on('pieceSelect', (piece) => {
      if(piece !== null) {
        this.selectedPiece = piece;
        this.emitter.emit('updateRequest');
      }
    });

    this.emitter.on('highlightSelect', (highlight) => {
      const currentSelectedPiece = this.selectedPiece;
      this.selectedPiece = null;
      this.emitter.emit('moveSelect', currentSelectedPiece + highlight);
    });
  }
}